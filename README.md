# How to compare two different literature work and write an essay on it
https://www.pexels.com/photo/anonymous-person-with-binoculars-looking-through-stacked-books-3769697/

An essay that requires you to compare two novels requires intelligence to write. Are you a [student](https://www.huffpost.com/entry/literary-fiction-vs-genre-fiction_b_4859609) and you do not know how to handle such papers? The following information will guide you through the process:

**Read the Two Texts**

Before you begin writing or creating a draft, it is crucial to know what the two literary works contain. Read and understand all the insights in the two books. Reading helps you make notes consciously. You get to understand the concepts of each author and their stand on the topic. Moreover, you can learn some aspects you did not know that might be essential in writing the essay.

**Develop a Basis of Comparison**

After reading the books, you now understand what they contain. The next step is to develop a basis for comparison. Developing this enables you to have a direction. Additionally, it will help you identify the differences and similarities more easily. For example, you may decide to compare the two literary works based on the themes. Therefore, you will check where the authors contrast each other in terms of themes. Also, you will be able to decipher similar themes in the two books.
Have you ever read fascinating books that you want to compare? You could go through this [source: Samplius](https://samplius.com/free-essay-examples/compare-and-contrast-between-i-have-a-dream-and-letter-from-birmingham-jail/), for free samples on compare and contrast essays. The writers have the experience I writing different types of essays. Hiring a writer is easy nowadays. However, some students still struggle with their essays. If you are stuck, simply get a writer to help you.

**Create a List of Similarities and Differences**

The basis of comparison will help you identify various similarities and contrasts in the text. The differences and similarities should focus based on the comparison. If you are focusing on themes, pick one theme and point out either a different or similar concept about it in the two books. Do this for all other themes as you develop a list you will use to create the final copy.

**Develop a Thesis Statement and Structure**

A thesis statement is the main argument. It should address both the differences and similarities. Consider both sides of the story and develop your stand. The structure of this essay may differ. The general structure of the essay should have an introduction, supporting paragraphs, and a conclusion. The thesis statement needs to be at the end of the introductory paragraph.

**Conclusion**

Essays comparing two literary works are not as easy as they may seem. Follow this procedure to do it right. Read the two books. Develop a [basis](https://www.forbes.com/sites/nextavenue/2016/03/09/how-to-improve-your-writing-skills-at-work/?sh=5d4a4f79899b) for comparison. After that, create a list of similarities and contrast. Lastly, develop a thesis statement and structure.
